#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>
#include <stdlib.h>

#include <zconf.h>

#define SHMSZ 80
int main() {
    char c;
    int shmid;
    key_t key;
    int *shm, *s;
    key = 56019;
    int temp;

    //create a shared memory
    if((shmid = shmget(key,SHMSZ,IPC_CREAT|0666))<0){
        printf("error");
        exit(1);
    }
    //attach to the process
    if((shm = shmat(shmid,NULL,0))== (int *) -1){
        printf("shmat error");
        exit(1);
    }
    s=shm;
    *shm = 0;


    for(int i =0; i<100; i++){
        s = shm + (4 * *shm);

        printf("shm (Size): %d\n",*shm);
        printf("address : %p: \n",s);
        //put no in increament
        *s = i;
        printf("produced %d\n",i);
        //increment the size identifier

        *shm = *shm+1;
        sleep(2);

    }
    putchar('\n');
    exit(0);
}