#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>
#include <stdlib.h>
#include <zconf.h>

#define SHMSZ  80

int main() {
    int shmid;
    key_t key;
    int *shm, *s;
    key = 56019;
    int currentIndex;
    //create a shared memory
    if ((shmid = shmget(key, SHMSZ, 0666)) < 0) {
        printf("shmget error");
        exit(1);
    }
    //attach a shared memory
    if ((shm = shmat(shmid, NULL, 0)) == (int *) -1) {
        printf("shmat error");
        exit(1);
    }

    s=shm;
    for(int i =0; i<100; i++){
        currentIndex = *shm;
        printf("pointer %p\n",s);
        s = shm + (4 * currentIndex);
        s=shm;
        //read from the buffer
        for(int j =0 ; j<currentIndex; j++){
            s = s + 4;
            if(*s != '#') {
                printf("consume %d \n", *s);
                *s = '#';
            }
        }

        printf("shm %d\n",*shm);

        sleep(3);

    }

    exit(0);
}